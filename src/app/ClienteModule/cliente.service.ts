import { Injectable } from '@angular/core';
import { MdPaginator } from '@angular/material';
import { Observable } from 'rxjs/Observable';
import { Observer } from 'rxjs/Observer';
import { Cliente, TIPO_CLIENTE } from './modelos/Cliente';

@Injectable()
export class ClienteService {
    
    clientes : Cliente[] = [];
    
    getClientes() : Observable<Cliente[]> {
        return Observable.create((observer) => {
            setTimeout(() => {
                observer.next(this.clientes);
                observer.complete();
            }, 200);
        });
    }
    
    criarCliente (cliente: Cliente) : Observable<boolean> {
        cliente.id = this.clientes.length + 1;
        this.clientes.push(cliente);
        return Observable.create((observer : Observer<boolean>) => {
            setTimeout(() => {
                observer.next(true);
                observer.complete();
            }, 200);
        });
    }

    getCliente(id) : Observable<Cliente> {
        return Observable.create(observer => {
            if(this.clientes.length)
                observer.next(this.clientes.find(item => item.id == id))
            else
                observer.next(null);
            observer.complete();
        });
    }

    removerCliente (id) : Observable<boolean> {
        let index;
        this.clientes.forEach((item, _index) => {
            if(item.id == id) {
                index = _index;
                 return false;
            } else {
                return true;
            }
        });
        if(index != null) {
            this.clientes.splice(index, 1);
        }
        return Observable.create(obs => {
            obs.next(true);
            obs.complete();
        });
    }

    updateCliente (cliente : Cliente) : Observable<boolean> {
        return Observable.create(obs => {
            let index;
            this.clientes.forEach((item, _index) => {
                if(item.id == cliente.id) {
                    index = _index;
                     return false;
                } else {
                    return true;
                }
            });
            if(index != null) {
                this.clientes[index] = cliente;
                obs.next(true);
            } else {
                obs.next(false);
            }
            obs.complete();
        });
    }
    
    
}