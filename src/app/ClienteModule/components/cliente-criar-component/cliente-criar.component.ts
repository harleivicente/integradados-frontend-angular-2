import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { ClienteService } from '../../cliente.service';
import { Cliente, TIPO_CLIENTE } from '../../modelos/Cliente';
import { MensagensService } from '../../../SharedModule/mensagens.service';

@Component({
    selector: 'cliente-criar',
    templateUrl: 'cliente-criar.component.html',
    styleUrls: ['cliente-criar.component.scss']
})

export class ClientCriarComponent implements OnInit {
    formularioCliente : FormGroup;
    aguardandoResposta : boolean = false;
    
    tipoPessoaSelectOptions = [
        {value: TIPO_CLIENTE.INTEGRADADOS, texto: 'Integradados'},
        {value: TIPO_CLIENTE.PUBLICIDADE, texto: 'Publicidade'}
    ];

    redeOpcoes = [
        {value: 1, texto: 'Rede 1'},
        {value: 2, texto: 'Rede 2'},
        {value: 3, texto: 'Rede 3'}
    ];

    constructor(
        private formBuilder : FormBuilder,
        private clienteServico : ClienteService,
        private router : Router,
        private message: MensagensService
    ) {
        this.criarFormulario();
    }

    ngOnInit() { 
        this.formularioCliente.reset();
    }

    criarFormulario() {
        this.formularioCliente = this.formBuilder.group({
            cnpjCpf: ['', Validators.required],
            inscricaoEstadual: '',
            inscricaoMunicipal: '',
            razaoSocial: '',
            nomeFantasia: '',
            cidade: '',
            endereco: '',
            uf: '',
            foneFinanceiro: '',
            emailFinanceiro: '',
            nomeContatoFinanceiro: '',
            foneEmpresaTI: '',
            emailEmpresaTI: '',
            razaoSocialEmpresaTI: '',
            nomeContatoEmpresaTI: '',
            codigoDaRede: ['', Validators.required],
            diaEmissaoNF: '',
            valorParcela: '',
            tipo: ['', Validators.required],
            segmentoComercial: '',
            comentario: ''
        });     
    }

    setAguardando(aguardando : boolean) {
        if(aguardando) {
            this.formularioCliente.disable();
            this.aguardandoResposta = true;
        } else {
            this.formularioCliente.enable();
            this.aguardandoResposta = false;
        }
    }

    onSubmit() {
        this.formularioCliente.get('codigoDaRede').markAsTouched();
        this.formularioCliente.get('tipo').markAsTouched();
        if(this.formularioCliente.valid) {
            this.setAguardando(true);
            let form_data = Object.assign({}, this.formularioCliente.value);
            let cliente = new Cliente(form_data);
            this.clienteServico.criarCliente(cliente).subscribe(ok => {
                this.setAguardando(false);
                if(ok){
                    this.message.inserirMensagem("Cliente criado com sucesso.");
                    this.router.navigate(['cliente']);
                } else {
                    this.message.inserirMensagem("Não foi possível criar o cliente.");
                }
            });
        } else  {
            this.message.inserirMensagem("Existem erros no preenchimento do formulário.");
        }

    }

    onCancel() {
        this.router.navigate(['cliente']);
    }


}