import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ClienteService } from '../../cliente.service';
import { Cliente } from '../../modelos/Cliente';
import { MensagensService } from '../../../SharedModule/mensagens.service';

@Component({
    selector: 'cliente-detalhes',
    templateUrl: 'cliente-detalhes.component.html',
    styleUrls: ['cliente-detalhes.component.scss']
})

export class ClienteDetalhesComponent implements OnInit {
    clienteForm : Cliente;
    _titulo : string = "";

    constructor(
        private route : ActivatedRoute,
        private router : Router,
        private clienteService: ClienteService,
        private message : MensagensService
    ) { }

    ngOnInit() { 
        this.route.params.subscribe(params => {
            this.carregarDados(params.id);
        });
    }

    carregarDados(id) {
        this.clienteService.getCliente(id).subscribe((cliente) => {
            if(cliente) {
                this.clienteForm = cliente;
                this._titulo = cliente.obterNome();
            } else {
                this.router.navigate(['cliente']);
                this.message.inserirMensagem("Esse cliente não existe.");
            }
        });        
    }

}