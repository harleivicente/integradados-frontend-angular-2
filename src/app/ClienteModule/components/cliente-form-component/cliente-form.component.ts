import { Component, OnInit, OnChanges, Input } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { ClienteService } from '../../cliente.service';
import { Cliente, TIPO_CLIENTE } from '../../modelos/Cliente';
import { MensagensService } from '../../../SharedModule/mensagens.service';

@Component({
    selector: 'cliente-form',
    templateUrl: 'cliente-form.component.html',
    styleUrls: ['cliente-form.component.scss']
})

export class ClienteFormComponent implements OnInit, OnChanges {
    formularioCliente : FormGroup;
    aguardandoResposta : boolean = false;
    @Input() cliente : Cliente | null;
    
    tipoPessoaSelectOptions = [
        {value: TIPO_CLIENTE.INTEGRADADOS, texto: 'Integradados'},
        {value: TIPO_CLIENTE.PUBLICIDADE, texto: 'Publicidade'}
    ];

    redeOpcoes = [
        {value: 1, texto: 'Rede 1'},
        {value: 2, texto: 'Rede 2'},
        {value: 3, texto: 'Rede 3'}
    ];
    
    constructor(
        private formBuilder : FormBuilder,
        private clienteServico : ClienteService,
        private router : Router,
        private message: MensagensService
    ) {
        this.criarFormulario();
    }

    ngOnInit() {}

    ngOnChanges() {
        if(this.cliente) {
            this.preecherFormulario(this.cliente);
        } else {
            this.formularioCliente.reset();
        }
    }

    criarFormulario() {
        this.formularioCliente = this.formBuilder.group({
            cnpjCpf: ['', Validators.required],
            inscricaoEstadual: '',
            inscricaoMunicipal: '',
            razaoSocial: '',
            nomeFantasia: '',
            cidade: '',
            endereco: '',
            uf: '',
            foneFinanceiro: '',
            emailFinanceiro: '',
            nomeContatoFinanceiro: '',
            foneEmpresaTI: '',
            emailEmpresaTI: '',
            razaoSocialEmpresaTI: '',
            nomeContatoEmpresaTI: '',
            codigoDaRede: ['', Validators.required],
            diaEmissaoNF: '',
            valorParcela: '',
            tipo: ['', Validators.required],
            segmentoComercial: '',
            comentario: ''
        });     
    }

    preecherFormulario(cliente : Cliente) {
        this.formularioCliente.setValue({
            cnpjCpf: cliente.cnpjCpf,
            inscricaoEstadual: cliente.inscricaoEstadual,
            inscricaoMunicipal: cliente.inscricaoMunicipal,
            razaoSocial: cliente.razaoSocial,
            nomeFantasia: cliente.nomeFantasia,
            cidade: cliente.cidade,
            endereco: cliente.endereco,
            uf: cliente.uf,
            foneFinanceiro: cliente.foneFinanceiro,
            emailFinanceiro: cliente.emailFinanceiro,
            nomeContatoFinanceiro: cliente.nomeContatoFinanceiro,
            foneEmpresaTI: cliente.foneEmpresaTI,
            emailEmpresaTI: cliente.emailEmpresaTI,
            razaoSocialEmpresaTI: cliente.razaoSocialEmpresaTI,
            nomeContatoEmpresaTI: cliente.nomeContatoEmpresaTI,
            codigoDaRede: cliente.codigoDaRede,
            diaEmissaoNF: cliente.diaEmissaoNF,
            valorParcela: cliente.valorParcela,
            tipo: cliente.tipo,
            segmentoComercial: cliente.segmentoComercial,
            comentario: cliente.comentario
        });
    }

    setAguardando(aguardando : boolean) {
        if(aguardando) {
            this.formularioCliente.disable();
            this.aguardandoResposta = true;
        } else {
            this.formularioCliente.enable();
            this.aguardandoResposta = false;
        }
    }

    onSubmit() {
        this.formularioCliente.get('codigoDaRede').markAsTouched();
        this.formularioCliente.get('tipo').markAsTouched();
        if(this.formularioCliente.valid) {

            // editando
            if(this.cliente) {
                let cliente_novo = Object.assign({}, this.cliente);
                let form_data = this.formularioCliente.value;
                Object.assign(cliente_novo, form_data);
                this.setAguardando(true);

                this.clienteServico.updateCliente(new Cliente(cliente_novo)).subscribe(ok => {
                    if(ok){
                        this.router.navigate(['cliente']);
                        this.message.inserirMensagem("Cliente atualizado.");
                    } else {
                        this.message.inserirMensagem("Ocorreu um erro no sistema.");
                    }
                });
            }

        } else {
            this.message.inserirMensagem("Existem erros no preenchimento do formuário.");
        }     
    }

    onCancel() {
        this.router.navigate(['cliente']);
    }    
}