import { Component, OnInit, Inject } from '@angular/core';
import {MdDialog, MdDialogRef} from '@angular/material';
import {MD_DIALOG_DATA} from '@angular/material';

@Component({
    selector: 'cliente-remover-confirmar',
    templateUrl: 'cliente-remover-confirmar.component.html',
    styleUrls: ['cliente-remover-confirmar.component.scss']
})

export class ClienteRemoverConfirmarComponent implements OnInit {
    constructor(
        public dialogRef: MdDialogRef<ClienteRemoverConfirmarComponent>,
        @Inject(MD_DIALOG_DATA) public data: any
    ) { }

    ngOnInit() { }
}