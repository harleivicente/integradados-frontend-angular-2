import { Component, OnInit, EventEmitter } from '@angular/core';
import { ClienteService } from '../../cliente.service';
import { Cliente } from '../../modelos/Cliente';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Observer } from 'rxjs/Observer';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import { MdDialogRef, MdDialog } from '@angular/material';
import {  } from '../../../SharedModule/mensagens.service';
import { ClienteRemoverConfirmarComponent } from '../cliente-remover-confirmar-component/cliente-remover-confirmar.component';
import { MensagensService } from '../../../SharedModule/mensagens.service';

@Component({
    selector: 'cliente-lista',
    templateUrl: 'cliente-lista.component.html',
    styleUrls: ['./cliente-lista.component.scss']
})

export class ClienteListaComponent implements OnInit {
    campos_tabela: {nome: string, texto: string}[] = [
        {nome: 'id', texto: 'Código'},
        {nome: 'cnpjCpf', texto: 'Cpf / Cnpj'},
        {nome: 'razaoSocial', texto: 'Razão social'},
        {nome: 'cidade', texto: 'Cidade'},
        {nome: 'foneFinanceiro', texto: 'Fone financeiro'},
        {nome: 'dataUltimoUpload', texto: 'Ultimo upload'}
    ];

    dados : Observable<any>;
    dadosBehaviorSubject : BehaviorSubject<any> = new BehaviorSubject([]);
    carregando : boolean = true;

    constructor( 
        private clienteService : ClienteService,
        private router : Router,
        private dialog : MdDialog,
        private messageService : MensagensService
    ) {
        this.dados = this.dadosBehaviorSubject.asObservable();
    }
    
    ngOnInit() {
        this.obterClientesServidor();
    }

    editar(item){
        this.router.navigate(['cliente', item.id]);
    }

    remover(item){
        this.dialog.open(
            ClienteRemoverConfirmarComponent,
            {
                data: item
            }
        ).afterClosed().subscribe(confirmado => {
            if(confirmado == "confirmar") {
                this.clienteService.removerCliente(item.id).subscribe(ok => {
                    if(ok){
                        this.obterClientesServidor();
                        this.messageService.inserirMensagem("Cliente remove com sucesso.");
                    } else {
                        this.messageService.inserirMensagem("Não foi possível remover o cliente.");
                    }
                });
            }
        });
    }

    obterClientesServidor() {
        this.carregando = true;
        this.clienteService.getClientes().subscribe((clientes) => {
            this.carregando = false;
            if(clientes)
                this.dadosBehaviorSubject.next(clientes);
        });
    }
}








    
