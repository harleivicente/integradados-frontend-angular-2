import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule, Routes } from '@angular/router';
import { CdkTableModule } from '@angular/cdk';

// Modulos
import { SharedModule } from '../SharedModule/shared.module';

// Servicos
import { ClienteService } from './cliente.service';
import { SessaoGuard } from '../SharedModule/sessao.guard';

// Componentes
import { ClienteListaComponent } from './components/cliente-lista-component/cliente-lista.component';
import { ClientCriarComponent } from './components/cliente-criar-component/cliente-criar.component';
import { ClienteComponent } from './components/cliente-component/cliente.component';
import { ClienteRemoverConfirmarComponent } from './components/cliente-remover-confirmar-component/cliente-remover-confirmar.component';
import { ClienteDetalhesComponent } from './components/cliente-detalhes-component/cliente-detalhes.component';
import { ClienteFormComponent } from './components/cliente-form-component/cliente-form.component';


const rotasClientes : Routes = [
    {
        path: 'cliente',
        component: ClienteComponent,
        canActivate: [SessaoGuard],
        children: [
            {path: '', component: ClienteListaComponent},
            {path: 'criar', component: ClientCriarComponent},
            {path: ':id', component: ClienteDetalhesComponent}
        ]
    }
];

@NgModule({
    imports: [
        SharedModule,
        BrowserModule,
        CdkTableModule,
        RouterModule.forChild(rotasClientes)
    ],
    declarations: [
        ClienteListaComponent,
        ClientCriarComponent,
        ClienteComponent,
        ClienteDetalhesComponent,
        ClienteFormComponent,
        ClienteRemoverConfirmarComponent
    ],
    providers: [
        ClienteService,
        SessaoGuard
    ],
    entryComponents: [
        ClienteRemoverConfirmarComponent
    ]
})

export class ClienteModule { }
