/**
* Cliente
*/

export enum TIPO_CLIENTE {
    INTEGRADADOS = "I",
    PUBLICIDADE = "P"
};

export class Cliente {
    id: string | number;
    cnpjCpf: string;
    inscricaoEstadual: string;
    inscricaoMunicipal: string;
    razaoSocial: string;
    nomeFantasia: string;
    cidade: string;
    endereco: string;
    uf: string;
    foneFinanceiro: string;
    emailFinanceiro: string;
    nomeContatoFinanceiro: string;
    foneEmpresaTI: string;
    emailEmpresaTI: string;
    razaoSocialEmpresaTI: string;
    nomeContatoEmpresaTI: string;
    codigoDaRede: string;
    status: boolean;
    dataDoCadastro: string;
    dataUltimoUpload: string;
    diaEmissaoNF: string;
    qtdeRegistroCadastrado: string;
    valorParcela: string;
    tipo: TIPO_CLIENTE;
    segmentoComercial: string;
    comentario: string;
    
    constructor(dados) {
        this.id = dados.id;
        this.cnpjCpf = dados.cnpjCpf;
        this.inscricaoEstadual = dados.inscricaoEstadual;
        this.inscricaoMunicipal = dados.inscricaoMunicipal;
        this.razaoSocial = dados.razaoSocial;
        this.nomeFantasia = dados.nomeFantasia;
        this.cidade = dados.cidade;
        this.endereco = dados.endereco;
        this.uf = dados.uf;
        this.foneFinanceiro = dados.foneFinanceiro;
        this.emailFinanceiro = dados.emailFinanceiro;
        this.nomeContatoFinanceiro = dados.nomeContatoFinanceiro;
        this.foneEmpresaTI = dados.foneEmpresaTI;
        this.emailEmpresaTI = dados.emailEmpresaTI;
        this.razaoSocialEmpresaTI = dados.razaoSocialEmpresaTI;
        this.nomeContatoEmpresaTI = dados.nomeContatoEmpresaTI;
        this.codigoDaRede = dados.codigoDaRede;
        this.status = dados.status;
        this.dataDoCadastro = dados.dataDoCadastro;
        this.dataUltimoUpload = dados.dataUltimoUpload;
        this.diaEmissaoNF = dados.diaEmissaoNF;
        this.qtdeRegistroCadastrado = dados.qtdeRegistroCadastrado;
        this.valorParcela = dados.valorParcela;
        this.tipo = dados.tipo;
        this.segmentoComercial = dados.segmentoComercial;
        this.comentario = dados.comentario;        
    }

    /**
     * Obtem algum nome para descrever a empresa.
     */
    obterNome() {
        let out = "";
        if(this.nomeFantasia)
            return this.nomeFantasia;
        if(this.razaoSocial)
            return this.razaoSocial;
        if(this.cnpjCpf)
            return this.cnpjCpf;
        return out;
    }

}