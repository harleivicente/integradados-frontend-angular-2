import {BehaviorSubject} from 'rxjs/BehaviorSubject';

/**
 * TableDatabase - Representa dados para serem
 * encapsulados em uma TableDataSource para fornecer
 * dados para um 'md-table' do angular material:
 * https://material.angular.io/components/table/overview
 */
export class TableDatabase {
  dataChange: BehaviorSubject<any> = new BehaviorSubject<any>([]);
  get data(): any[] { return this.dataChange.value; }

  constructor(dados : any) {
      this.dataChange.next(dados);
  }
}