import { DataSource } from '@angular/cdk';
import { MdPaginator } from '@angular/material';
import { TableDatabase } from './TableDatabase';
import { Observable } from 'rxjs/Observable';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/observable/merge';
import 'rxjs/add/operator/map';

/**
 * @param _database : TableDatabase
 * @param _paginator : MdPaginator
 * @param _campo_filtro : string[] - Lista dos nomes dos campos
 * que devem fazer parte da filtragem.
 */
export class TableDataSource extends DataSource<any> {
    _mudancaNoFiltro = new BehaviorSubject('');

    get filtro(): string { return this._mudancaNoFiltro.value; }
    set filtro(filtro: string) { this._mudancaNoFiltro.next(filtro); }

  constructor(
        private _database: TableDatabase,
        private _paginator: MdPaginator,
        private _campo_filtro : string[]
    ) {
    super();
  }

  connect(): Observable<any> {
    const mudancaNosDadosMostrados = [
        this._database.dataChange,
        this._paginator.page,
        this._mudancaNoFiltro
    ];

    return Observable.merge(...mudancaNosDadosMostrados).map(
        () => {
            let dados = this._database.data.slice();
            let valores_campos_filtrados;


            // Filtragem dos dados
            dados = dados.filter((item) => {
                valores_campos_filtrados = "";

                // coletar dados dos campos filtrados
                this._campo_filtro.forEach((value) => {
                    valores_campos_filtrados += item[value];
                });

                let searchStr = (valores_campos_filtrados).toLowerCase();
                return searchStr.indexOf(this.filtro.toLowerCase()) != -1;
            });

            // Paginacao dos dados
            const startIndex = this._paginator.pageIndex * this._paginator.pageSize;
            return dados.splice(startIndex, this._paginator.pageSize);
        }
    );
  }

  disconnect() {}
}
