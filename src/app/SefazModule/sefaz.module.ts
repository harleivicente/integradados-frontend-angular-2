import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule, Routes } from '@angular/router';

// Modulos
import { SharedModule } from '../SharedModule/shared.module';
import { SessaoGuard } from '../SharedModule/sessao.guard';
import { SefazService } from './sefaz.service';
import { FileSelectDirective } from 'ng2-file-upload';
import { SefazComponent } from './components/sefaz-component/sefaz.component';
import { SefazContribuintesComponent } from './components/sefaz-contribuintes-component/sefaz-contribuintes.component';

const rotas : Routes = [
    {
        path: "sefaz",
        component: SefazComponent,
        canActivate: [SessaoGuard],
        children: [
            {
                path: '',
                component: SefazContribuintesComponent
            }
        ]
    }
];

@NgModule({
    imports: [
        BrowserModule,
        SharedModule,
        RouterModule.forChild(rotas)        
    ],
    exports: [],
    declarations: [
        FileSelectDirective,
        SefazComponent,
        SefazContribuintesComponent
    ],
    providers: [
        SefazService
    ],
})
export class SefazModule { }
