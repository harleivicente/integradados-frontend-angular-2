import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Observer } from 'rxjs/Observer';
import { ApiService } from '../SharedModule/api.service';
import { ContribuinteSefaz } from './modelos/ContribuinteSefaz';

@Injectable()
export class SefazService {
    contribuintes : ContribuinteSefaz[] = [];

    constructor(
        private apiService : ApiService
    ) { 
        this.contribuintes.push(new ContribuinteSefaz({cnpj: 123, id: 1, uf: 'GO'}));
        this.contribuintes.push(new ContribuinteSefaz({cnpj: 1234, id: 2, uf: 'SP'}));
    }

    getContribuintes() : Observable<ContribuinteSefaz[]> {
        return Observable.create(obs => {
            setTimeout(() => {
                obs.next(this.contribuintes);
                obs.complete();
            }, 200);
        });
    }

}