import { Component, OnInit, OnChanges } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Observer } from 'rxjs/Observer';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import { MensagensService } from '../../../SharedModule/mensagens.service';
import { SefazService } from '../../sefaz.service';
import { FileUploader, FileUploaderOptions } from 'ng2-file-upload';

@Component({
    selector: 'sefaz-contribuintes',
    templateUrl: 'sefaz-contribuintes.component.html',
    styleUrls: ['sefaz-contribuintes.component.scss']
})

export class SefazContribuintesComponent implements OnInit, OnChanges {
    public uploader:FileUploader = new FileUploader(
        {
            url: 'https://evening-anchorage-3159.herokuapp.com/api/'
        }
    );
    enviando : boolean = false;
    carregando : boolean = true;

    campos_tabela = [
        {nome: 'id', texto: 'Código'},
        {nome: 'cnpj', texto: 'CNPJ'},
        {nome: 'razaoSocial', texto: 'Razão Social'},
        {nome: 'uf', texto: 'UF'},
        {nome: 'cnae', texto: 'CNAE'}
    ];
    
    cidadeOptions = [
        {value: 'para', texto: 'Pará'},
        {value: 'goias', texto: 'Goiás'}
    ];

    dados: Observable<any>;
    dadosBehaviorSubject : BehaviorSubject<any> = new BehaviorSubject([]);
    
    constructor(
        private router : Router,
        private messageService : MensagensService,
        private sefazServico : SefazService
    ) { 
        this.dados = this.dadosBehaviorSubject.asObservable();
    }
    
    obterDadosServidor() {
        this.sefazServico.getContribuintes().subscribe(dados => {
            this.carregando = false;
            if(dados) {
                this.dadosBehaviorSubject.next(dados);
            }
        });
    }
    
    ngOnInit() { 
        this.obterDadosServidor();
    }
    
    enviar() {
        this.enviando = true;
        setTimeout(() => {
            this.enviando = false;
        }, 4000);
    }
    
    ngOnChanges() {}
}