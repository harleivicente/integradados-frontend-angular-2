

export class ContribuinteSefaz {
    id: string | number;
    cnpj: string;
    inscricaoEstadual: string;
    razaoSocial: string;
    cnae: string;
    regimeApuracaoImposto;
    dataCadastroSefaz;
    uf : string;

    constructor(dados) {
        this.id = dados.id;
        this.cnpj = dados.cnpj;
        this.inscricaoEstadual = dados.inscricaoEstadual;
        this.razaoSocial = dados.razaoSocial;
        this.cnae = dados.cnae;
        this.uf = dados.uf;
    }

}