import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { SessaoService } from './sessao.service';

@Injectable()
export class SessaoGuard implements CanActivate {
  
  constructor(private sessao : SessaoService, private router : Router) {}
  
  canActivate() {
    if(this.sessao.usuario.value != null) {
      return true;
    } else {
      this.router.navigate(['logar']);
      return false;
    }
  }
}

