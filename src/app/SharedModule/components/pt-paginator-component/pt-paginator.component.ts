import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { MdPaginator } from '@angular/material';

@Component({
    selector: 'pt-paginator',
    templateUrl: 'pt-paginator.component.html'
})

export class PtPaginatorComponent implements OnInit {
    constructor() { }
    @Input() numeroDeItems = 0;
    @ViewChild('paginator') pagintor : MdPaginator;

    ngOnInit() { 
        this.pagintor._intl.nextPageLabel = "Próximo página";
        this.pagintor._intl.previousPageLabel = "Página anterior";
        this.pagintor._intl.getRangeLabel = (page, pageSize, length) => {
            let resto = length % pageSize;
            let parteInteira = (length - resto) / pageSize;
            let numero_paginas;

            if(resto > 0) {
                numero_paginas = parteInteira + 1;
            } else {
                numero_paginas = parteInteira;
            }

            return "Página " + (page+1) + " de " + numero_paginas;
        };
        this.pagintor._intl.itemsPerPageLabel = "Items por página";
    }
}