import { Component, OnInit, ChangeDetectionStrategy, ViewChild,  ElementRef, Input, EventEmitter } from '@angular/core';
import { MdSidenav } from '@angular/material';
import { ActivatedRoute, UrlSegment, Router } from '@angular/router';

// Servicos
import { MensagensService } from '../../mensagens.service';

interface ConfiguracaoMenu {
    textoCategoria: string,
    items : {
        url: string,
        texto: string,
        urlMatch: RegExp,
        nome: string
    }[]
}

@Component({
    selector: 'layout',
    templateUrl: 'layout.component.html',
    styleUrls: ['layout.component.scss']
})
export class LayoutComponent implements OnInit {
    @ViewChild('sidenav') sidenav;
    @Input() carregando : boolean;

    // Controla o formato da navegacao lateral
    mode : String;

    // Mensagens globais
    mensagens : string [] = [];

    // Nome do menu atual
    menuAtualNome : string = "";
    menuItemAtual : {url: string, texto: string, nome: string} = {url:'', texto: '', nome: ''};

    private menuNavegacao : ConfiguracaoMenu[] = [
        {
            textoCategoria: 'Clientes',
            items: [
                {
                    nome: 'criar-cliente',
                    urlMatch: /^\/cliente\/criar$/,
                    url: '/cliente/criar',
                    texto: 'Cadastro cliente'
                },
                {
                    nome: 'lista-cliente',
                    urlMatch: /^\/cliente$/,
                    url: '/cliente',
                    texto: 'Lista de clientes'
                }
            ]
        },
        {
            textoCategoria: 'Usuários',
            items: [
                {
                    nome: 'cadastro-usuario',
                    urlMatch: /^\/usuario\/cadastro$/,
                    url: '/usuario/cadastro',
                    texto: 'Cadastro de Usuário'
                },
                {
                    nome: 'lista-usuario',
                    urlMatch: /^\/usuario$/,
                    url: '/usuario',
                    texto: 'Lista de usuários'
                }
            ]
        },
        {
            textoCategoria: 'Redes',
            items: [
                {
                    nome: 'cadastro-rede',
                    urlMatch: /^\/rede\/cadastro$/,
                    url: '/rede/cadastro',
                    texto: 'Cadastro de rede'
                },
                {
                    nome: 'lista-rede',
                    urlMatch: /^\/rede$/,
                    url: '/rede',
                    texto: 'Lista de redes'
                }
            ]
        },
        {
            textoCategoria: 'SEFAZ',
            items: [
                {
                    nome: 'sefaz',
                    urlMatch: /^\/sefaz$/,
                    url: '/sefaz',
                    texto: 'Contribuintes SEFAZ'
                }
            ]
        },
        {
            textoCategoria: 'Terceiros',
            items: [
                {
                    nome: 'restrito',
                    urlMatch: /^\/restrito$/,
                    url: '/restrito',
                    texto: 'Terceiros restritos'
                }
            ]
        }               
    ];

    constructor(
        private elementRef : ElementRef, 
        private route : ActivatedRoute,
        private router : Router, 
        private mensagensService : MensagensService ) {}
        
        ngOnInit() {
            this.mensagensService.mensagems.subscribe((mensagens) => {
                this.mensagens = mensagens;
            });

            // Atualiza menu selecionado
            this.route.url.subscribe(() => {
                let current_route = this.router.url;
                let current = '';
                let currentMenuItem = null;

                this.menuNavegacao.forEach(configMenu => {
                    configMenu.items.forEach(menuItem => {
                        if(menuItem.urlMatch.test(current_route)) {
                            current = menuItem.nome;
                            currentMenuItem = menuItem;
                        }
                    });
                });
                this.menuAtualNome = current;
                this.menuItemAtual = currentMenuItem;
            });

            this.resize();
    }

    toggle() {
        this.sidenav.toggle();
    }
    
    /**
     * Ajusta tamanho da navegacao lateral.
     */
    private resize() {
        var rootElement = this.elementRef.nativeElement;
        var target = rootElement.getElementsByTagName('md-sidenav-container')[0];

        if(target.clientWidth >= 1024) {
            this.mode = "side";
            this.sidenav.open();
        } else {
            this.mode = "over";
            this.sidenav.close();
        }
    }
    
}