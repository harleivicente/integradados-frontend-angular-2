import { Component, OnInit, Input } from '@angular/core';

interface entidadeSimples {
    [s: string]: string | number; 
}


@Component({
    selector: 'lista-entidade-simples',
    templateUrl: 'lista-entidade-simples.component.html',
    styleUrls: ['lista-entidade-simples.component.scss']
})
export class ListaEntidadeSimplesComponent implements OnInit {
    constructor() { }
    @Input() propriedades : string[] = [];
    @Input() entidades : entidadeSimples[] = [];

    ngOnInit() { 
    }
}