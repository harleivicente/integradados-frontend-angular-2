import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { SessaoService } from '../../sessao.service';
import { Usuario } from '../../../UsuarioModule/modelos/Usuario';
import { MdMenuTrigger } from '@angular/material';

@Component({
    selector: 'menu-usuario',
    templateUrl: 'menu-usuario.component.html',
    styleUrls: ['menu-usuario.component.scss']
})
export class MenuUsuarioComponent implements OnInit {
    usuario : Usuario;
    @ViewChild('notificationTrigger') trigger : MdMenuTrigger;

    constructor(
        private sessao : SessaoService,
        private router : Router
    ) { 
        this.sessao.usuario.subscribe(usuario => {
            this.usuario = usuario;
        });
    }

    ngOnInit() { 
    }

    sair() {
        this.sessao.finalizarSessao();
        this.router.navigate(['logar']);
    }

    notificationClick(event : Event) {
        event.cancelBubble = true;
    }    

    removerNotificacao() {
        this.trigger.closeMenu();
    }
}