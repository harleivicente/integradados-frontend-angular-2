import { Component, OnInit, OnChanges, Input, Output, EventEmitter } from '@angular/core';
import { DataSource } from '@angular/cdk';
import { Observable } from 'rxjs/Observable';

interface EntidadeSimples {
    [chave:string] : string | number | boolean;
}


/**
 * @input acoes string[] - Possiveis acoes: 'editar', 'detalhes', 'remover'
 * Só serão emitidos eventos para ações configuradas nesse parametro.
 */
@Component({
    selector: 'tabela-entidade',
    templateUrl: 'tabela-entidade.component.html',
    styleUrls: ['tabela-entidade.component.scss']
})
export class TabelaEntidadeComponent implements OnInit {
    @Input() colunas : {nome: string, texto: string}[] = [];
    @Input() dados : Observable<EntidadeSimples[]>;
    @Input() acoes : string[] = [];

    @Output() editarItem : EventEmitter<EntidadeSimples> = new EventEmitter();
    @Output() removerItem : EventEmitter<EntidadeSimples> = new EventEmitter();
    @Output() detalhesItem : EventEmitter<EntidadeSimples> = new EventEmitter();

    fonteDados: TableDataSource | null;
    // Colunas visiveis
    colunaNomes: string[] = [];

    constructor() { }

    ngOnInit() { 
        // Adicionar columa de acoes de edicao
        if(this.acoes.length) {
            this.colunas.push({nome: 'acoes', texto: 'Acões'});
        }
        
        this.colunaNomes = [];
        this.colunas.forEach(value => {
            this.colunaNomes.push(value.nome);
        });
        this.fonteDados = new TableDataSource(this.dados); 
    }

    edit(linha : EntidadeSimples) {
        this.editarItem.emit(linha);
    }

    remove(linha : EntidadeSimples) {
        this.removerItem.emit(linha);
    }

    detalhes(linha : EntidadeSimples) {
        this.detalhesItem.emit(linha);
    }
}

export class TableDataSource extends DataSource<any> {
    private dados : Observable<any>;

    constructor(dados : Observable<any>) {
        super();
        this.dados = dados;
    }

    connect(): Observable<any> {
        return this.dados;
    }

    disconnect() {}
}