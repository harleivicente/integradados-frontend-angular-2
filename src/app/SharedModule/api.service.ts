import { Injectable } from '@angular/core';
import { Http, Response }       from '@angular/http';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs/Observable';
import { Observer } from 'rxjs/Observer';

@Injectable()
export class ApiService {
    
    constructor(private http : Http) {}
    
    /**
    * Faz requisição para a api integradados. Retorna null caso ocorra algum
    * problema.
    * 
    * @param method
    * @param url : 'usuarios/novo'
    */
    makeApiCall(method : string, url : string, data = {} ) : Observable<Response> {
        let out;
        url = environment.apiUrl + url;
        
        switch (method) {
            case 'get':
            out = this.http.get(url).catch(this.handleError);
            break;
            case 'put':
            out = this.http.put(url, data).catch(this.handleError);
            break;
            case 'post':
            out = this.http.post(url, data).catch(this.handleError);
            break;
            case 'delete':
            out = this.http.delete(url).catch(this.handleError);
            break;
        }
        
        return out;
    }

    handleError (response : Response, observable : Observable<any>) {
        if(response.ok) {
            return observable;
        } else {
            return Observable.create((obs) => {
                obs.next(null);
                obs.complete();
            });
        }
    }
}