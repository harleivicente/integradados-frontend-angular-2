import { Injectable } from '@angular/core';
import { Usuario } from '../UsuarioModule/modelos/Usuario';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class SessaoService {
    usuario : BehaviorSubject<Usuario>;
    token : string;

    constructor() { 
        this.usuario = new BehaviorSubject(null);
    }

    iniciarSessao(usuario : Usuario, token : string) {
        this.usuario.next(usuario);
        this.token = token;
    }

    finalizarSessao() {
        this.usuario.next(null);
        this.token = null;
    }
}