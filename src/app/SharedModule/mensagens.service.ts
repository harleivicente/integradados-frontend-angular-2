import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Observer } from 'rxjs/Observer';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';

@Injectable()
export class MensagensService {
    private _mensagens: string[] = [];
    mensagems : BehaviorSubject<string[]>;

    constructor() {
        this.mensagems = new BehaviorSubject<string[]>([]);
    }

    inserirMensagem(mensagem : string) {
        this._mensagens.push(mensagem);
        this.revelarMensagem();
        this.mensagems.next(this._mensagens);
        setTimeout(() => {
            this.removerMensagem(mensagem);
        }, 4000);
    }

    /**
     * Ajusta o scroll da tela para revelar a mensagem.
     */
    revelarMensagem() {
        let elements = document.getElementsByClassName("mat-sidenav-content");
        if(elements && elements.length)
            elements[0].scrollTop = 0;
    }

    /**
     * Remove uma mensagem da pilha. Caso
     * tenha mais de uma, remove a mais velha.
     */
    removerMensagem(mensagem : string) {
        for (var index = this._mensagens.length-1; index >= 0; index--) {
            var item = this._mensagens[index];
            var foundIndex = null;
            if(item == mensagem) {
                foundIndex = index;
                break;
            }            
        }
        if(foundIndex != null) {
            this._mensagens.splice(foundIndex, 1);
        }
        this.mensagems.next(this._mensagens);
    }


}