import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { JsonPipe } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';
import { CdkTableModule } from '@angular/cdk';

// Modulos
import { 
    MdIconModule, MdButtonModule,
    MdSidenavModule, MdListModule,
    MdTableModule, MdPaginatorModule,
    MdInputModule, MdSelectModule,
    MdDatepickerModule, MdNativeDateModule,
    MdCheckboxModule, MdDialogModule,
    MdDialogRef, MdTabsModule,
    MdProgressSpinnerModule,
    MdMenuModule, MdProgressBarModule
} from '@angular/material';

// Componentes
import { LayoutComponent } from './components/layout-component/layout.component';
import { PtPaginatorComponent } from './components/pt-paginator-component/pt-paginator.component';
import { ListaEntidadeSimplesComponent } from './components/lista-entidade-simples-component/lista-entidade-simples.component';
import { TabelaEntidadeComponent } from './components/tabela-entidade-component/tabela-entidade.component';
import { MenuUsuarioComponent } from './components/menu-usuario-component/menu-usuario.component';

// Servicos
import { MensagensService } from './mensagens.service';
import { SessaoService } from './sessao.service';
import { ApiService } from './api.service';

import { SessaoGuard } from './sessao.guard';

@NgModule({
    imports: [
        RouterModule,
        MdPaginatorModule,
        BrowserModule,
        MdButtonModule,
        MdMenuModule,
        MdIconModule,
        MdSidenavModule,
        MdListModule,
        MdTableModule,
        MdInputModule,
        MdSelectModule,
        FormsModule,
        CdkTableModule,
        HttpModule,
        MdTabsModule,
        MdProgressBarModule,
        MdProgressSpinnerModule,
        MdDialogModule,
        ReactiveFormsModule,
        BrowserAnimationsModule
    ],
    exports: [
        LayoutComponent,
        MdTableModule,
        MdPaginatorModule,
        MdInputModule,
        MdMenuModule,
        MdButtonModule,
        MdIconModule,
        MdSelectModule,
        MdTabsModule,
        MdProgressSpinnerModule,
        PtPaginatorComponent,
        TabelaEntidadeComponent,
        MdProgressBarModule,
        MenuUsuarioComponent,
        ListaEntidadeSimplesComponent,
        FormsModule,
        HttpModule,
        MdDatepickerModule,
        CdkTableModule,
        MdNativeDateModule,
        ReactiveFormsModule,
        BrowserAnimationsModule,
        MdDialogModule,
        MdCheckboxModule
    ],
    declarations: [
        LayoutComponent,
        PtPaginatorComponent,
        ListaEntidadeSimplesComponent,
        TabelaEntidadeComponent,
        MenuUsuarioComponent
    ],
    providers: [
        JsonPipe,
        MensagensService,
        SessaoService,
        ApiService
    ],
})
export class SharedModule { }

