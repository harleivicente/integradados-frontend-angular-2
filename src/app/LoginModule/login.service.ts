import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class LoginService {

    users = [
        {usuario: 'admin', senha: 'admin'},
        {usuario: 'gestor', senha: 'gestor'}
    ];

    constructor() { }

    login(usuario, senha) : Observable<boolean> {
        return Observable.create(obs => {
            setTimeout(()=>{
                obs.next(this.verificarCredenciais(usuario, senha));
                obs.complete();
            }, 200);
        });
    }

    verificarCredenciais(usuario ,senha) : boolean {
        let item = null;
        item = this.users.find(item => {
            return item.usuario == usuario
        });
        return item && item.senha == senha;
    }
}