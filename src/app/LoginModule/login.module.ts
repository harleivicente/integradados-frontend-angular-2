import { NgModule, Component } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../SharedModule/shared.module';
import { CommonModule } from '@angular/common';  
import { BrowserModule } from '@angular/platform-browser';

// Components
import { LoginComponent } from './login-component/login.component'

// Servico
import { LoginService } from './login.service';

const rotasLogin: Routes = [
  { path: 'logar',  component: LoginComponent},
];

@NgModule({
    declarations: [
        LoginComponent
    ],
    imports: [
        CommonModule,
        BrowserModule,
        SharedModule,
        RouterModule.forChild(rotasLogin)
    ],
    providers: [
        LoginService
    ]
    }
)
export class LoginModule {}