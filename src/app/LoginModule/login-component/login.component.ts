import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { MensagensService } from '../../SharedModule/mensagens.service';
import { LoginService } from '../login.service';
import { Router } from '@angular/router';
import { SessaoService } from '../../SharedModule/sessao.service';
import { Usuario, TIPO_USUARIO } from '../../UsuarioModule/modelos/Usuario';

@Component({
    selector: 'login',
    templateUrl: 'login.component.html',
    styleUrls: ['login.component.scss']
})

export class LoginComponent implements OnInit {
    loginForm : FormGroup;
    msg : string = "";
    aguardando : boolean = false;

    constructor(
        private formBuilder : FormBuilder,
        private messageService : MensagensService,
        private loginService : LoginService,
        private router : Router,
        private sessao : SessaoService
    ) {
        this.construirFormulario();
    }
    
    ngOnInit() { 
        this.messageService.mensagems.subscribe(msgs => {
            if(msgs.length)
                this.msg = msgs[msgs.length-1];
            else
                this.msg = "";
        });
    }

    construirFormulario() {
        this.loginForm = this.formBuilder.group({
            usuario: ['', Validators.required],
            senha: ['', Validators.required]
        });
    }

    definirAguardando(aguardando : boolean) {
        if(aguardando) {
            this.loginForm.disable();
            this.aguardando = true;
        } else {
            this.loginForm.enable();
            this.aguardando = false;
        }
    }

    onSubmit() {
        if(this.loginForm.valid) {
            let form_data = this.loginForm.value;
            this.definirAguardando(true);
            this.loginService.login(form_data.usuario, form_data.senha).subscribe(ok => {
                this.definirAguardando(false);
                if(ok) {
                    let usuario = new Usuario({id: 1, nomeCompleto: 'Usuario teste', categoria: TIPO_USUARIO.ADMINISTRADOR});
                    this.sessao.iniciarSessao(usuario, 'token');
                    this.router.navigate(['cliente']);
                } else {
                    this.messageService.inserirMensagem("Credenciais incorretas.");
                }
            });
        }
    }

}