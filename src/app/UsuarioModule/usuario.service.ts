import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { Observer } from 'rxjs/Observer';
import { ApiService } from '../SharedModule/api.service';

import { Usuario } from './modelos/Usuario';
import { Cliente } from '../ClienteModule/modelos/Cliente';

@Injectable()
export class UsuarioService {
    _usuarios : Usuario[] = [];

    constructor(private apiService : ApiService) { }

    cadastrarUsuario(usuario : Usuario, cliente : Cliente) : Observable<Usuario> {
        let dados = {
            nomeCompleto: usuario.nomeCompleto,
            nomeAbreviado: usuario.nomeAbreviado,
            descricaoFuncao: usuario.descricaoFuncao,
            senha: usuario.senha,
            categoria: usuario.categoria,
            sexo: usuario.sexo,
            email: usuario.email,
            telefoneFixo: usuario.telefoneFixo,
            telefoneCelular: usuario.telefoneCelular,
            cliente: cliente.id,
            codigoCadastrador: usuario.codigoCadastrador,
            informacoesAdicionais: usuario.informacoesAdicionais
        };

        return this.apiService.makeApiCall('put', 'usuarios/novo', dados).map((response) => {
            if(response) {
                return new Usuario(response);
            } else {
                return null;
            }
        });
    }

    obterUsuarios() : Observable<Usuario[]> {
        return this.apiService.makeApiCall('get', 'usuarios').map((response) => {
            if(response){
                let data = response.json();
                let usuarios : Usuario[] = [];
                if(data.content.length) {
                    data.content.forEach((usuario) => {
                        usuarios.push(new Usuario(usuario));
                    })
                }
                return usuarios;
            } else {
                return null;
            }
        });
    }

    removerUsuario(id) : Observable<boolean> {
        let url = 'usuarios/' + id.toString();
        return this.apiService.makeApiCall('delete', url).map((response) => {
            if(response) {
                return true;
            } else {
                return false;
            }
        });
    }

    obterUsuario(id) : Observable<Usuario> {
        return this.apiService.makeApiCall('get', `usuarios/${id}`).map((response) => {
            if(response) {
                let data = response.json();
                return new Usuario(data);
            } else {
                return null;
            }
        });
    }

    editarUsuario(usuario : Usuario) : Observable<boolean> {
        let dados = {
            id: usuario.id,
            nomeCompleto: usuario.nomeCompleto,
            nomeAbreviado: usuario.nomeAbreviado,
            descricaoFuncao: usuario.descricaoFuncao,
            senha: usuario.senha,
            categoria: usuario.categoria,
            sexo: usuario.sexo,
            email: usuario.email,
            telefoneFixo: usuario.telefoneFixo,
            telefoneCelular: usuario.telefoneCelular,
            cliente: usuario.cliente,
            codigoCadastrador: usuario.codigoCadastrador,
            informacoesAdicionais: usuario.informacoesAdicionais
        };

        return this.apiService.makeApiCall('put', `usuarios/novo`, dados).map((response) => {
            if(response) {
                return true;
            } else {
                return false;
            }
        });
    }

}