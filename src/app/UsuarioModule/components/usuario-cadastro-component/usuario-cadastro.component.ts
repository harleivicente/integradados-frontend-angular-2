import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { Usuario, TIPO_USUARIO } from '../../modelos/Usuario';
import { Router } from '@angular/router';
import { Cliente } from '../../../ClienteModule/modelos/Cliente';
import { Http, Response }       from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Observer } from 'rxjs/Observer';

// Servico
import { UsuarioService } from '../../usuario.service';
import { MensagensService } from '../../../SharedModule/mensagens.service';

@Component({
    selector: 'usuario-cadastro',
    templateUrl: 'usuario-cadastro.component.html',
    styleUrls: ['usuario-cadastro.component.scss'],
    providers: []
})

export class UsuarioCadastroComponent implements OnInit {
    formularioUsuario: FormGroup;
    formularioCliente: FormGroup;
    aguardandoResposta: boolean = false;

    tipoUsuarioOptions : {texto: string, value: string | number}[] = [];
    sexoOptions : {texto: string, value: string | number}[] = [];    

    redeOptions : {viewValue: string, value: string | number}[] = [
        { viewValue: "Rede A", value: 101 },
        { viewValue: "Rede B", value: 102 },
        { viewValue: "Rede C", value: 103 }
    ];

    clienteOptions : {viewValue: string, value: string | number}[] = [
        { viewValue: "Cliente A", value: 31 },
        { viewValue: "Cliente B", value: 32 },
        { viewValue: "Cliente C", value: 33 }
    ];

    constructor(
        private formBuilder : FormBuilder,
        private router : Router,
        private messageService : MensagensService,
        private http : Http,
        private usuarioServico: UsuarioService) {
        this.criarFormularios();
    }

    ngOnInit() { 
        this.tipoUsuarioOptions = Usuario.obterTiposUsuario();
        this.sexoOptions = [{texto: 'M', value: 'M'}, {texto: 'F', value: 'F'}];
    }

    criarFormularios() {
        this.formularioUsuario = this.formBuilder.group({
            categoria: ['', Validators.required],
            sexo: ['', Validators.required],
            nomeCompleto: ['', Validators.required],
            nomeAbreviado: ['', Validators.required],
            email: ['', Validators.required],
            descricaoFuncao: ['', Validators.required],
            telefoneFixo: ['', Validators.required],
            telefoneCelular: ['', Validators.required],
            senha: ['', Validators.required],
            senha_confirmar: ['', Validators.required],
            informacoesAdicionais: ['', Validators.required]
        });
        this.formularioCliente = this.formBuilder.group({
            rede: ['', Validators.required],
            cliente: ['', Validators.required]
        });
    }

    /**
     * Configura página de modo que usuário tem que aguardar resposta
     * do servidor.
     */
    modeDeEspera(ativo : boolean) {
        if(ativo) {
            this.aguardandoResposta = true;
            this.formularioCliente.disable();
            this.formularioUsuario.disable();
        } else {
            this.aguardandoResposta = false;
            this.formularioCliente.enable();
            this.formularioUsuario.enable();
        }
    }

    onCancel() {
        this.router.navigate(['usuario']);
    }

    onSubmit() {
        if(this.formularioUsuario.valid &&
            this.formularioCliente.valid) {
                let form_data = Object.assign({}, this.formularioUsuario.value);
                form_data.codigoCadastrador = 1313;
                let user : Usuario = new Usuario(form_data);
                let cliente : Cliente = new Cliente({});

                this.modeDeEspera(true);
                this.usuarioServico.cadastrarUsuario(user, cliente).subscribe((resultado) => {
                    this.modeDeEspera(false);
                    if(resultado) {
                        this.router.navigate(['usuario']);
                        this.messageService.inserirMensagem("Usuário inserido com sucesso.");
                    } else {
                        this.messageService.inserirMensagem("Não foi possível criar o usuário.");
                    }
                });

                this.http.put("http://localhost:8080/usuarios/novo", {}).catch((e, obs) => {
                    console.log(e);
                    return Observable.create(a => {
                        a.next('3434');
                    });
                });

            } else {
                this.messageService.inserirMensagem("Verique os campos do formulário.");
            }
    }
}