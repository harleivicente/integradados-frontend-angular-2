import { Component, OnInit, Inject } from '@angular/core';
import {MdDialog, MdDialogRef} from '@angular/material';
import {MD_DIALOG_DATA} from '@angular/material';

@Component({
    selector: 'usuario-remover-confirmar',
    templateUrl: 'usuario-remover-confirmar.component.html',
    styleUrls: ['usuario-remover-confirmar.component.scss']
})

export class UsuarioRemoverConfirmarComponent implements OnInit {
    constructor(
        public dialogRef: MdDialogRef<UsuarioRemoverConfirmarComponent>,
        @Inject(MD_DIALOG_DATA) public data: any
    ) { }

    ngOnInit() { }
}