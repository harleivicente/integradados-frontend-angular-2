import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';
import { Observer } from 'rxjs/Observer';
import { MensagensService } from '../../../SharedModule/mensagens.service';


import { MdDialogRef, MdDialog } from '@angular/material';
import { Usuario } from '../../modelos/Usuario';
import { UsuarioService } from '../../usuario.service';
import { UsuarioRemoverConfirmarComponent } from '../usuario-remover-confirmar-component/usuario-remover-confirmar.component';

@Component({
    selector: 'usuario-lista',
    templateUrl: 'usuario-lista.component.html',
    styleUrls: ['usuario-lista.component.scss']
})

export class UsuarioListaComponent implements OnInit {
    campos_tabela : {nome: string, texto: string}[] = [
        {nome: 'id', texto: 'Código'},
        {nome: 'nomeAbreviado', texto: 'Nome Abreviado'},
        {nome: 'categoria', texto: 'Categoria'},
        {nome: 'ativo', texto: 'Status'},
        {nome: 'descricaoFuncao', texto: 'Função'},
        {nome: 'dataCadastro', texto: 'Data de cadastro'}
    ];
    dados : Observable<any>;
    dadosBehaviorSubject : BehaviorSubject<any> = new BehaviorSubject([]);
    carregando : boolean = true;

    constructor(
        private usuarioService : UsuarioService,
        private dialog : MdDialog,
        private router : Router,
        private messagemService : MensagensService
    ) {
        this.dados = this.dadosBehaviorSubject.asObservable();
    }

    ngOnInit() { 
        this.obterUsuarioDoServidor();
    }

    obterUsuarioDoServidor() {
        this.carregando = true;
        this.usuarioService.obterUsuarios().subscribe(usuarios => {
            this.carregando = false;
            if(usuarios)
                this.dadosBehaviorSubject.next(usuarios);
        });        
    }

    removerUsuario(usuario) {
        let dialogRef = this.dialog.open(
            UsuarioRemoverConfirmarComponent, {
            data: usuario
        });
        dialogRef.afterClosed().subscribe(result => {
            if(result == "confirmar") {
                this.usuarioService.removerUsuario(usuario.id).subscribe(ok => {
                    if(ok){
                        this.obterUsuarioDoServidor();
                        this.messagemService.inserirMensagem(`Usuário ${usuario.nomeCompleto} foi removido.`);
                    } else {
                        this.messagemService.inserirMensagem(`Não foi possível remover o usuário ${usuario.nomeCompleto}.`);
                    }
                });
            }
        });        
    }

    editarUsuario(usuario) {
        this.router.navigate(['usuario', usuario.id]);
    }
    
}