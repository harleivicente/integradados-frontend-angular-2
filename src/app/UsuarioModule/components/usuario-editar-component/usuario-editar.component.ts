import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { Usuario, TIPO_USUARIO } from '../../modelos/Usuario';
import { Router, ActivatedRoute } from '@angular/router';
import { Cliente } from '../../../ClienteModule/modelos/Cliente';
import { Http, Response }       from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Observer } from 'rxjs/Observer';

// Servico
import { UsuarioService } from '../../usuario.service';
import { MensagensService } from '../../../SharedModule/mensagens.service';

@Component({
    selector: 'usuario-editar',
    templateUrl: 'usuario-editar.component.html',
    styleUrls: ['usuario-editar.component.scss']
})

export class UsuarioEditarComponent implements OnInit {
    formularioUsuario: FormGroup;
    formularioCliente: FormGroup;
    aguardandoResposta: boolean = false;
    usuarioEditado: Usuario;
    _titulo : string = "";

    tipoUsuarioOptions : {texto: string, value: string | number}[] = [];
    sexoOptions : {texto: string, value: string | number}[] = [];    

    redeOptions : {viewValue: string, value: string | number}[] = [
        { viewValue: "Rede A", value: 101 },
        { viewValue: "Rede B", value: 102 },
        { viewValue: "Rede C", value: 103 }
    ];

    clienteOptions : {viewValue: string, value: string | number}[] = [
        { viewValue: "Cliente A", value: 31 },
        { viewValue: "Cliente B", value: 32 },
        { viewValue: "Cliente C", value: 33 }
    ];

    constructor(
        private formBuilder : FormBuilder,
        private router : Router,
        private messageService : MensagensService,
        private http : Http,
        private route: ActivatedRoute,
        private usuarioServico: UsuarioService) {
        this.criarFormularios();
    }

    ngOnInit() { 
        this.tipoUsuarioOptions = Usuario.obterTiposUsuario();
        this.sexoOptions = [{texto: 'M', value: 'M'}, {texto: 'F', value: 'F'}];
        let params = this.route.snapshot.params as any;

        this.usuarioServico.obterUsuario(params.id).subscribe(usuario => {
            if(usuario) {
                this._titulo = usuario.nomeCompleto;
                this.usuarioEditado = usuario;
                this.preencherFormulario(usuario);
            } else {
                this.messageService.inserirMensagem("Esse usuário não existe.");
                this.router.navigate(['usuario']);
            }
        });
    }

    preencherFormulario(usuario : Usuario) {
        let usuario_form_data = {
            categoria: usuario.categoria,
            sexo: usuario.sexo,
            nomeCompleto: usuario.nomeCompleto,
            nomeAbreviado: usuario.nomeAbreviado,
            email: usuario.email,
            descricaoFuncao: usuario.descricaoFuncao,
            telefoneFixo: usuario.telefoneFixo,
            telefoneCelular: usuario.telefoneCelular,
            senha: usuario.senha,
            senha_confirmar: "",
            informacoesAdicionais: usuario.informacoesAdicionais
        };

        let cliente_formulario = {
            rede: 101,
            cliente: 31
        };
        this.formularioUsuario.setValue(usuario_form_data);
        this.formularioCliente.setValue(cliente_formulario);
    }

    criarFormularios() {
        this.formularioUsuario = this.formBuilder.group({
            categoria: ['', Validators.required],
            sexo: ['', Validators.required],
            nomeCompleto: ['', Validators.required],
            nomeAbreviado: ['', Validators.required],
            email: ['', Validators.required],
            descricaoFuncao: ['', Validators.required],
            telefoneFixo: ['', Validators.required],
            telefoneCelular: ['', Validators.required],
            senha: ['', Validators.required],
            senha_confirmar: [''],
            informacoesAdicionais: ['', Validators.required]
        });
        this.formularioCliente = this.formBuilder.group({
            rede: ['', Validators.required],
            cliente: ['', Validators.required]
        });
    }

    /**
     * Configura página de modo que usuário tem que aguardar resposta
     * do servidor.
     */
    modeDeEspera(ativo : boolean) {
        if(ativo) {
            this.aguardandoResposta = true;
            this.formularioCliente.disable();
            this.formularioUsuario.disable();
        } else {
            this.aguardandoResposta = false;
            this.formularioCliente.enable();
            this.formularioUsuario.enable();
        }
    }

    onCancel() {
        this.router.navigate(['usuario']);
    }

    onSubmit() {
        if(this.formularioUsuario.valid &&
            this.formularioCliente.valid) {
                let form_data = Object.assign({}, this.formularioUsuario.value);
                form_data.codigoCadastrador = 1313;
                form_data.id = this.usuarioEditado.id;
                let user : Usuario = new Usuario(form_data);

                this.modeDeEspera(true);
                this.usuarioServico.editarUsuario(user).subscribe((resultado) => {
                    this.modeDeEspera(false);
                    if(resultado) {
                        this.router.navigate(['usuario']);
                        this.messageService.inserirMensagem("Usuário editaco com sucesso.");
                    } else {
                        this.messageService.inserirMensagem("Não foi possível editar o usuário.");
                    }
                });

            } else {
                this.messageService.inserirMensagem("Verique os campos do formulário.");
            }
    }
}