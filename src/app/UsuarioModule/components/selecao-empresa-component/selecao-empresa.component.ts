import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';

@Component({
    selector: 'selecao-empresa',
    templateUrl: 'selecao-empresa.component.html',
    styleUrls: ['selecao-empresa.component.scss']
})

export class SelecaoEmpresaComponent implements OnInit {
    formularioCliente: FormGroup;

    redeOptions : {viewValue: string, value: string | number}[] = [
        { viewValue: "Rede A", value: 101 },
        { viewValue: "Rede B", value: 102 },
        { viewValue: "Rede C", value: 103 }
    ];

    clienteOptions : {viewValue: string, value: string | number}[] = [
        { viewValue: "Cliente A", value: 31 },
        { viewValue: "Cliente B", value: 32 },
        { viewValue: "Cliente C", value: 33 }
    ];

    constructor(
        private formBuilder : FormBuilder
    ) { }

    ngOnInit() {
        this.criarFormularios();
    }

    criarFormularios() {
        this.formularioCliente = this.formBuilder.group({
            rede: ['', Validators.required],
            cliente: ['', Validators.required]
        });
    }    
}