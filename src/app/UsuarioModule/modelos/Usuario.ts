
export enum TIPO_USUARIO {
    ADMINISTRADOR = "A",
    GESTOR = "G",
    MASTER = "M",
    COLABORADOR = "C"
}

export class Usuario {
    id: string;
    nomeCompleto: string;
    nomeAbreviado: string;
    descricaoFuncao: string;
    senha: string;
    dataExpiracao: string;
    ativo: string;
    categoria: TIPO_USUARIO;
    sexo: string; // M ou F
    dataNascimento: string;
    email: string;
    telefoneFixo: string;
    telefoneCelular: string;
    cliente: string;
    codigoCadastrador: string;
    ultimoAcesso: string;
    dataCadastro: string;
    codigoUsuarioInativador: string;
    dataInativacao: string;
    motivoInativacao: string;
    mensagem: string;
    dataValidadeMensagem: string;
    informacoesAdicionais: string;
    
    constructor(dados) {
        this.id = dados.id;
        this.nomeCompleto = dados.nomeCompleto;
        this.nomeAbreviado = dados.nomeAbreviado;
        this.descricaoFuncao = dados.descricaoFuncao;
        this.senha = dados.senha;
        this.dataExpiracao = dados.dataExpiracao;
        this.ativo = dados.ativo;
        this.categoria = dados.categoria;
        this.sexo = dados.sexo; // M ou F
        this.dataNascimento = dados.dataNascimento;
        this.email = dados.email;
        this.telefoneFixo = dados.telefoneFixo;
        this.telefoneCelular = dados.telefoneCelular;
        this.cliente = dados.cliente;
        this.codigoCadastrador = dados.codigoCadastrador;
        this.ultimoAcesso = dados.ultimoAcesso;
        this.dataCadastro = dados.dataCadastro;
        this.codigoUsuarioInativador = dados.codigoUsuarioInativador;
        this.dataInativacao = dados.dataInativacao;
        this.motivoInativacao = dados.motivoInativacao;
        this.mensagem = dados.mensagem;
        this.dataValidadeMensagem = dados.dataValidadeMensagem;
        this.informacoesAdicionais = dados.informacoesAdicionais;
    }
    
    static obterTiposUsuario() : {texto: string, value: string}[] {
        let out = [
            {texto: 'Administrador', value: TIPO_USUARIO.ADMINISTRADOR},
            {texto: 'Gestor', value: TIPO_USUARIO.GESTOR},
            {texto: 'Master', value: TIPO_USUARIO.MASTER},
            {texto: 'Colaborador', value: TIPO_USUARIO.COLABORADOR}
        ];
        return out;
    }

}