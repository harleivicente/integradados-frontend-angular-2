import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule, Routes } from '@angular/router';
import { SessaoGuard } from '../SharedModule/sessao.guard';

// Modulos
import { SharedModule } from '../SharedModule/shared.module';

// Componentes
import { UsuarioCadastroComponent } from './components/usuario-cadastro-component/usuario-cadastro.component';
import { UsuarioComponent } from './components/usuario-component/usuario.component';
import { UsuarioListaComponent } from './components/usuario-lista-component/usuario-lista.component';
import { SelecaoEmpresaComponent } from './components/selecao-empresa-component/selecao-empresa.component';
import { UsuarioRemoverConfirmarComponent } from './components/usuario-remover-confirmar-component/usuario-remover-confirmar.component';
import { UsuarioEditarComponent } from './components/usuario-editar-component/usuario-editar.component';

// Servicos
import { UsuarioService } from './usuario.service';

const rotasUsuario : Routes = [
    {
        path: "usuario",
        component: UsuarioComponent,
        canActivate: [SessaoGuard],
        children: [
            {
                path: '',
                component: UsuarioListaComponent
            },
            {
                path: 'cadastro',
                component: UsuarioCadastroComponent
            },
            {
                path: ':id',
                component: UsuarioEditarComponent
            }
        ]
    }
];

@NgModule({
    imports: [
        BrowserModule,
        SharedModule,
        RouterModule.forChild(rotasUsuario)
    ],
    exports: [],
    declarations: [
        UsuarioComponent,
        UsuarioCadastroComponent,
        UsuarioListaComponent,
        SelecaoEmpresaComponent,
        UsuarioRemoverConfirmarComponent,
        UsuarioEditarComponent
    ],
    entryComponents: [
        UsuarioRemoverConfirmarComponent
    ],
    providers: [
        UsuarioService,
        SessaoGuard      
    ],
})
export class UsuarioModule { }
