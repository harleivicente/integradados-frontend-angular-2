import { Socio } from './Socio';

export enum TIPO_PESSOA {
    FISICA = "F",
    JURIDICA = "J"
};

export class Terceiro {
    id : string | number;
    codigoRede: string | number;
    razaoSocial : string;
    nomeFantasia : string;
    cpfCnpj : string;
    endereco : string;
    cidade : string;
    uf : string;
    tipoPessoa : TIPO_PESSOA;
    informacoesAdicionais: string;
    codigoClienteInformante: string | number;
    socios : Socio[] = [];

    constructor(dados) {
        this.id = dados.id;
        this.codigoRede = dados.codigoRede;
        this.razaoSocial = dados.razaoSocial;
        this.nomeFantasia = dados.nomeFantasia;
        this.cpfCnpj = dados.cpfCnpj;
        this.endereco = dados.endereco;
        this.cidade = dados.cidade;
        this.uf = dados.uf;
        this.tipoPessoa = dados.tipoPessoa;
        this.informacoesAdicionais = dados.informacoesAdicionais;
        this.codigoClienteInformante = dados.codigoClienteInformante;
        
        if(dados.socios) {
            dados.socios.forEach(socio => {
                this.socios.push(new Socio(socio));
            });
        }
    }

}