
export class TerceiroRestrito {
    id : string | number;
    cpfCnpj : string;
    codigoRede : string | number;
    razaoSocial : string;
    nomeFantasia : string;
    motivoRestricao : string;
    dataRestricao : string;
    usuarioRestricao : string;

    constructor(dados) {
        this.id = dados.id; 
        this.cpfCnpj = dados.cpfCnpj; 
        this.codigoRede = dados.codigoRede; 
        this.razaoSocial = dados.razaoSocial; 
        this.nomeFantasia = dados.nomeFantasia; 
        this.motivoRestricao = dados.motivoRestricao; 
        this.dataRestricao = dados.dataRestricao; 
        this.usuarioRestricao = dados.usuarioRestricao;         
    }
}