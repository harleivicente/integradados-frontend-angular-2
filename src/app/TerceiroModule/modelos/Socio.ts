/**
 * Socio
 */

export class Socio {
    id : string | number;
    cpf: string;
    cnpjCpfTerceiro : string;
    cargo : string;
    nome : string;
    percentualSociedade : number;
    infoAdicional : string;

    constructor(dados) {
        this.id = dados.id;
        this.cpf = dados.cpf;
        this.cnpjCpfTerceiro = dados.cnpjCpfTerceiro;
        this.cargo = dados.cargo;
        this.nome = dados.nome;
        this.percentualSociedade = dados.percentualSociedade;
        this.infoAdicional = dados.infoAdicional;
    }
}