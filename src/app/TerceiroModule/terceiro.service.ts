import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { Observer } from 'rxjs/Observer';
import { ApiService } from '../SharedModule/api.service';

import { TerceiroRestrito } from './modelos/TerceiroRestrito';

@Injectable()
export class TerceiroService {
    _restritos : TerceiroRestrito[] = [];

    constructor() { 
        this._restritos.push(new TerceiroRestrito({
            id: 3, 
            razaoSocial : 'Social',
            motivoRestricao: 'Sem motivo'
        }));
    }

    obterTerceirosRestritos() : Observable<TerceiroRestrito[]> {
        return Observable.create((obs) => {
            setTimeout(() => {
                obs.next(this._restritos);
                obs.complete();
            }, 200);
        });
    }


}