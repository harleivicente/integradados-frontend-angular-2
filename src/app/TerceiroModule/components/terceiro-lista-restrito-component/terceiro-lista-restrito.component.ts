import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';
import { Observer } from 'rxjs/Observer';
import { MensagensService } from '../../../SharedModule/mensagens.service';

import { Terceiro } from '../../modelos/Terceiro';
import { TerceiroService } from '../../terceiro.service';

@Component({
    selector: 'terceiro-lista-restrito',
    templateUrl: 'terceiro-lista-restrito.component.html',
    styleUrls: ['terceiro-lista-restrito.component.scss']
})

export class TerceiroListaRestritoComponent implements OnInit {
    campos_tabela : {nome: string, texto: string}[] = [
        {nome: 'id', texto: 'Código'},
        {nome: 'cpfCnpj', texto: 'CPF/CNPJ'},
        {nome: 'razaoSocial', texto: 'Razão Social'},
        {nome: 'motivoRestricao', texto: 'Motivo'},
        {nome: 'dataRestricao', texto: 'Data da restrição'}
    ];

    dados : Observable<any>;
    dadosBehaviorSubject : BehaviorSubject<any> = new BehaviorSubject([]);
    carregando : boolean = true;

    constructor(
        private usuarioService : TerceiroService,
        private messagemService : MensagensService,
        private terceiroService : TerceiroService,
        private router : Router
    ) { 
        this.dados = this.dadosBehaviorSubject.asObservable();
    }

    ngOnInit() {
        this.obterDadosServidor();
    }

    obterDadosServidor() {
        this.terceiroService.obterTerceirosRestritos().subscribe(dados => {
            this.carregando = false;
            this.dadosBehaviorSubject.next(dados);
        });
    }

    removerItem() {}

    editarItem() {}

}