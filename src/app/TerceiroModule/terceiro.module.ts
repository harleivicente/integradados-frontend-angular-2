import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule, Routes } from '@angular/router';
import { SessaoGuard } from '../SharedModule/sessao.guard';
import { SharedModule } from '../SharedModule/shared.module';
import { TerceiroService } from './terceiro.service';
import { TerceiroListaRestritoComponent } from './components/terceiro-lista-restrito-component/terceiro-lista-restrito.component';

const rotas : Routes = [
    {
        path: "restrito",
        component: TerceiroListaRestritoComponent,
        canActivate: [SessaoGuard]
    }
];

@NgModule({
    imports: [
        BrowserModule,
        SharedModule,
        RouterModule.forChild(rotas)         
    ],
    exports: [],
    declarations: [
        TerceiroListaRestritoComponent
    ],
    providers: [
        TerceiroService
    ],
})
export class TerceiroModule { }
