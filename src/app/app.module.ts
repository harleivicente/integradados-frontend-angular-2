import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { RouterModule, Routes } from '@angular/router';

// Modules
import { SharedModule } from './SharedModule/shared.module';
import { LoginModule } from './LoginModule/login.module';
import { ClienteModule } from './ClienteModule/cliente.module';
import { UsuarioModule } from './UsuarioModule/usuario.module';
import { RedeModule } from './RedeModule/rede.module';
import { SefazModule } from './SefazModule/sefaz.module';
import { TerceiroModule } from './TerceiroModule/terceiro.module';

// Components
import { NaoEncontradoComponent } from './nao-encontrado-component/nao-encontrado.component';
import { AppComponent } from './app-component/app.component';

const rotasAplicacao: Routes = [
  { path: '', redirectTo: '/cliente', pathMatch: 'full' },
  {path: '**', component: NaoEncontradoComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    NaoEncontradoComponent
  ],
  imports: [
    BrowserModule,

    // Modulos de funcionalidade
    SharedModule,
    LoginModule,
    ClienteModule,
    UsuarioModule,
    RedeModule,
    SefazModule,
    TerceiroModule,
    RouterModule.forRoot(rotasAplicacao)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
