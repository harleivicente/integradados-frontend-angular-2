

export class Rede {
    id: string | number;
    nome: string;
    sequenciaFaixaControlada: number;
    diasParaUploadArquivo: number;
    qtdeDiasControlePgto: number;
    qtdeDiasValidadeDados: number;
    validaUpload : boolean;
    /**
     * json of array of numbers: "[1,12]"
     */
    descricaoPrimeiraFaixa: string;
    descricaoSegundaFaixa: string;
    descricaoTerceiraFaixa: string;
    descricaoQuartaFaixa: string;

    constructor(dados) {
        this.id = dados.id;
        this.nome = dados.nome;
        this.sequenciaFaixaControlada = dados.sequenciaFaixaControlada;
        this.diasParaUploadArquivo = dados.diasParaUploadArquivo;
        this.qtdeDiasControlePgto = dados.qtdeDiasControlePgto;
        this.qtdeDiasValidadeDados = dados.qtdeDiasValidadeDados;
        this.validaUpload  = dados.validaUpload;
        this.descricaoPrimeiraFaixa = dados.descricaoPrimeiraFaixa;
        this.descricaoSegundaFaixa = dados.descricaoSegundaFaixa;
        this.descricaoTerceiraFaixa = dados.descricaoTerceiraFaixa;
        this.descricaoQuartaFaixa = dados.descricaoQuartaFaixa;
    }

    /**
     * @param numero - 1 - 4
     * @param inicio - numero
     * @param fim - numero
     * 
     * Inicio e fim devem ser ambos definidos para que 
     * a faixa seja definida. Além disso, ambos os valores
     * devem ser maiores que zero.
     */
    setDescricaoFaixa(numero, inicio, fim) {
        switch (numero) {
            case 1:
                this.descricaoPrimeiraFaixa = JSON.stringify([inicio, fim]);
                break;
            case 2:
                this.descricaoSegundaFaixa = JSON.stringify([inicio, fim]);
                break;
            case 3:
                this.descricaoTerceiraFaixa = JSON.stringify([inicio, fim]);
                break;
            case 4:
                this.descricaoQuartaFaixa = JSON.stringify([inicio, fim]);
                break;
        }
    }

}
