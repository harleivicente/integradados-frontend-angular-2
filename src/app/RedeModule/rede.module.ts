import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule, Routes } from '@angular/router';
import { SessaoGuard } from '../SharedModule/sessao.guard';

// Servicos
import { RedeService } from './rede.service';

// Modulos
import { SharedModule } from '../SharedModule/shared.module';

// Componentes
import { RedeComponent } from './components/rede-component/rede.component';
import { RedeEditarComponent } from './components/rede-editar-component/rede-editar.component';
import { RedeListaComponent } from './components/rede-lista-component/rede-lista.component';
import { RemoverRedeConfirmarComponent } from './components/rede-remover-confirmar-component/rede-remover-confirmar.component';

const rotas : Routes = [
    {
        path: "rede",
        component: RedeComponent,
        canActivate: [SessaoGuard],
        children: [
            {
                path: '',
                component: RedeListaComponent
            },
            {
                path: 'cadastro',
                component: RedeEditarComponent
            },
            {
                path: ':id',
                component: RedeEditarComponent
            }
        ]
    }
];

@NgModule({
    imports: [
        BrowserModule,
        SharedModule,
        RouterModule.forChild(rotas)        
    ],
    exports: [],
    declarations: [
        RedeListaComponent,
        RedeComponent,
        RedeEditarComponent,
        RemoverRedeConfirmarComponent
    ],
    providers: [
        RedeService
    ],
    entryComponents: [
        RemoverRedeConfirmarComponent
    ]
})
export class RedeModule { }
