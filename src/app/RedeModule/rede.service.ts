import { ApiService } from '../SharedModule/api.service';
import { Observable } from 'rxjs/Observable';
import { Observer } from 'rxjs/Observer';
import { Injectable } from '@angular/core';
import { Rede } from './modelos/Rede';
import 'rxjs/add/operator/map';

@Injectable()
export class RedeService {
    redes : Rede[] = [];

    constructor() {
        this.redes.push(new Rede({id:1, nome: 'Rede de mercados'}));
        // this.redes.push(new Rede({id:2, nome: 'Rede de cosmeticos'}));
    }

    createRede(rede : Rede) : Observable<boolean> {
        rede.id = (this.redes.length + 1);
        this.redes.push(rede);
        return Observable.create(obs => {
            setTimeout(()=>{
                obs.next(rede);
                obs.complete();
            }, 200);
        });
    }

    getRedes() : Observable<Rede[]> {
        return Observable.create(obs => {
            setTimeout(() => {
                obs.next(this.redes);
                obs.complete();
            }, 200);
        });
    }

    getRede(id) : Observable<Rede> {
        return Observable.create(obs => {
            setTimeout(() => {
                let found = null;
                found = this.redes.find(item => item.id == id);
                obs.next(found);
                obs.complete();
            }, 200);
        });
    }

    editarRede(rede : Rede) : Observable<boolean> {
        return Observable.create((obs) => {
            let index = this.redes.findIndex(item => item.id == rede.id);
            if(index != null)
                this.redes[index] = rede;
            obs.next(index != null);
            obs.complete();
        });
    }

    removerRede(id) : Observable<boolean> {
        return Observable.create((obs) => {
            let index = this.redes.findIndex(item => item.id == id);
            if(index != null)
                this.redes.splice(index, 1);
            obs.next(true);
            obs.complete();
        });        
    }
}