import { Component, OnInit, Inject } from '@angular/core';
import {MdDialog, MdDialogRef} from '@angular/material';
import {MD_DIALOG_DATA} from '@angular/material';

@Component({
    selector: 'rede-remover-confirmar',
    templateUrl: 'rede-remover-confirmar.component.html',
    styleUrls: ['rede-remover-confirmar.component.scss']
})

export class RemoverRedeConfirmarComponent implements OnInit {
    
    constructor(
        public dialogRef: MdDialogRef<RemoverRedeConfirmarComponent>,
        @Inject(MD_DIALOG_DATA) public data: any
    ) { }

    ngOnInit() { }
}