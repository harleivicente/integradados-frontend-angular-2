import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { Rede } from '../../modelos/Rede';
import { RedeService } from '../../rede.service';
import { MensagensService } from '../../../SharedModule/mensagens.service';

@Component({
    selector: 'rede-editar',
    templateUrl: 'rede-editar.component.html',
    styleUrls: ['rede-editar.component.scss']
})

export class RedeEditarComponent implements OnInit {
    aguardandoResposta : boolean = false;
    formularioRede : FormGroup;
    redeSendoEditada : Rede;

    constructor(
        private formBuilder : FormBuilder,
        private messageService : MensagensService,
        private redeService : RedeService,
        private router : Router,
        private route : ActivatedRoute
    ) { }

    ngOnInit() { 
        this.criarFormulario();

        this.route.params.subscribe(route => {
            let params = route as any;

            if(params.id) {
                this.redeService.getRede(params.id).subscribe(rede => {
                    if(rede) {
                        this.redeSendoEditada = rede;
                        this.preencherFormulario(rede);
                    } else {
                        this.messageService.inserirMensagem("Essa Rede não existe.");
                        this.router.navigate(['rede']);
                    }
                });
            } else {
                this.redeSendoEditada = null;
                this.formularioRede.reset();
            }
        });
    }

    preencherFormulario(rede : Rede) {
        this.formularioRede.setValue({
            nome: rede.nome,
            inicioPrimeiraFaixa: JSON.parse(rede.descricaoPrimeiraFaixa)[0],
            fimPrimeiraFaixa: JSON.parse(rede.descricaoPrimeiraFaixa)[1],
            inicioSegundaFaixa: JSON.parse(rede.descricaoSegundaFaixa)[0],
            fimSegundaFaixa: JSON.parse(rede.descricaoSegundaFaixa)[1],
            inicioTerceiraFaixa: JSON.parse(rede.descricaoTerceiraFaixa)[0],
            fimTerceiraFaixa: JSON.parse(rede.descricaoTerceiraFaixa)[1],
            inicioQuartaFaixa: JSON.parse(rede.descricaoQuartaFaixa)[0],
            fimQuartaFaixa: JSON.parse(rede.descricaoQuartaFaixa)[1]
        });
    }

    criarFormulario() {
        this.formularioRede = this.formBuilder.group({
            nome: ['', Validators.required],
            inicioPrimeiraFaixa: '',
            fimPrimeiraFaixa: '',
            inicioSegundaFaixa: '',
            fimSegundaFaixa: '',
            inicioTerceiraFaixa: '',
            fimTerceiraFaixa: '',
            inicioQuartaFaixa: '',
            fimQuartaFaixa: ''
        });     
    }

    setAguardando(aguardando : boolean) {
        if(aguardando) {
            this.formularioRede.disable();
            this.aguardandoResposta = true;
        } else {
            this.formularioRede.enable();
            this.aguardandoResposta = false;
        }
    }    

    onCancel() {
        this.router.navigate(['rede']);
    }

    onSubmit() {

        if(this.formularioRede.valid) {

            // EDITANDO REDE EXISTENTE
            if(this.redeSendoEditada) {
                let rede_atual_dados = Object.assign({}, this.redeSendoEditada);
                let form_data = Object.assign({}, this.formularioRede.value);
                Object.assign(rede_atual_dados, form_data);
                let rede_nova = new Rede(rede_atual_dados);
                
                rede_nova.setDescricaoFaixa(1, form_data.inicioPrimeiraFaixa, form_data.fimPrimeiraFaixa);
                rede_nova.setDescricaoFaixa(2, form_data.inicioSegundaFaixa, form_data.fimSegundaFaixa);
                rede_nova.setDescricaoFaixa(3, form_data.inicioTerceiraFaixa, form_data.fimTerceiraFaixa);
                rede_nova.setDescricaoFaixa(4, form_data.inicioQuartaFaixa, form_data.fimQuartaFaixa);

                this.setAguardando(true);
                this.redeService.editarRede(rede_nova).subscribe(ok => {
                    this.setAguardando(false);
                    if (ok) {
                        this.messageService.inserirMensagem("Rede atualizada com sucesso.");
                        this.router.navigate(['rede']);
                    } else {
                        this.messageService.inserirMensagem("Nâo foi possível editar essa rede.");
                    }
                });

            // CRIANDO NOVA REDE
            } else {
                this.setAguardando(true);
                let form_data = Object.assign({}, this.formularioRede.value);
                let rede = new Rede(form_data);

                // definir faixas
                rede.setDescricaoFaixa(1, form_data.inicioPrimeiraFaixa, form_data.fimPrimeiraFaixa);
                rede.setDescricaoFaixa(2, form_data.inicioSegundaFaixa, form_data.fimSegundaFaixa);
                rede.setDescricaoFaixa(3, form_data.inicioTerceiraFaixa, form_data.fimTerceiraFaixa);
                rede.setDescricaoFaixa(4, form_data.inicioQuartaFaixa, form_data.fimQuartaFaixa);

                this.redeService.createRede(rede).subscribe(ok => {
                    if(ok) {
                        this.messageService.inserirMensagem("Rede criada com sucesso.");
                        this.router.navigate(['rede']);
                    } else {
                        this.messageService.inserirMensagem("Não foi possível criar a rede.")
                    }
                });
            }
        } else {
            this.messageService.inserirMensagem("Existem erros no preenchimento do formulário.");
        }
    }


}