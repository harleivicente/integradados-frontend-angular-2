import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import { Router, Route } from '@angular/router';
import { MdDialogRef, MdDialog } from '@angular/material';

// Component
import { RemoverRedeConfirmarComponent } from '../rede-remover-confirmar-component/rede-remover-confirmar.component';

// Servicos
import { RedeService } from '../../rede.service';
import { MensagensService } from '../../../SharedModule/mensagens.service';

@Component({
    selector: 'rede-lista',
    templateUrl: 'rede-lista.component.html',
    styleUrls: ['rede-lista.component.scss']
})

export class RedeListaComponent implements OnInit {
    campos_tabela = [
        {nome: 'id', texto: 'Código'},
        {nome: 'nome', texto: 'Nome'}
    ];

    dados : Observable<any>;
    dadosBehaviourSubject : BehaviorSubject<any> = new BehaviorSubject([]);
    carregando : boolean = true;

    constructor(
        private redeServico : RedeService,
        private router : Router,
        private dialog : MdDialog,
        private messageService : MensagensService
    ) { 
        this.dados = this.dadosBehaviourSubject.asObservable();
    }

    ngOnInit() {
        this.obterDadosDoServidor();
    }

    obterDadosDoServidor() {
        this.carregando = true;
        this.redeServico.getRedes().subscribe(redes => {
            this.carregando = false;
            this.dadosBehaviourSubject.next(redes);
        });        
    }

    editarItem(item) {
        this.router.navigate(['rede', item.id]);
    }

    removerItem(item) {
        this.dialog.open(
            RemoverRedeConfirmarComponent,
            {data: item}
        ).afterClosed().subscribe(confirmado => {
            if(confirmado == "confirmar") {
                this.redeServico.removerRede(item.id).subscribe(ok => {
                    if(ok) {
                        this.obterDadosDoServidor();
                        this.messageService.inserirMensagem("Rede removida com sucesso.");
                    } else {
                        this.messageService.inserirMensagem("Nâo foi possível remove a rede.");
                    }
                });
            }
        });
    }
}