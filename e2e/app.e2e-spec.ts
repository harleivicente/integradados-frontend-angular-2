import { IntegradadosFrontEndPage } from './app.po';

describe('integradados-front-end App', () => {
  let page: IntegradadosFrontEndPage;

  beforeEach(() => {
    page = new IntegradadosFrontEndPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
